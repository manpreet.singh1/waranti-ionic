import { NgModule } from '@angular/core';
import { NavSideBarComponent} from './nav-sidebar/nav-sidebar.component';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule} from '@ionic/angular';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NavSideBarComponent],
  imports:[
    BrowserModule,
    IonicModule,
    RouterModule
  ],
  exports:[
    NavSideBarComponent
  ]
})
export class LayoutModule {}
