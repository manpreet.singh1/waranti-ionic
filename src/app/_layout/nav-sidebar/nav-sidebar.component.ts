import { Component } from '@angular/core';

@Component({
  selector: 'nav-sidebar',
  templateUrl: 'nav-sidebar.component.html'
})
export class NavSideBarComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'My Waranties',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'Exciting Offers',
      url: '/list',
      icon: 'color-wand'
    },
    {
      title: 'My Profile',
      url: '/profile',
      icon: 'person'
    },
    {
      title: 'Logout',
      url: '/login',
      icon: 'exit'
    }
  ];
}
