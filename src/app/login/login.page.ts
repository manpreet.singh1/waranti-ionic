import { Component, OnInit } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
import { LoadingController, AlertController,NavController} from '@ionic/angular';
import { LoginServices } from '../../app/login/login.api.service';

import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  model= {}
  submitModel={};
  
    constructor(public navCtrl: NavController,public alertController: AlertController,public menuCtrl: MenuController,  public platform: Platform,public api: LoginServices,public loadingController: LoadingController,  public route: ActivatedRoute,
    public router: Router) { }
  
    ngOnInit() {
    this.menuCtrl.enable(false);
    }

 async loginUser() {
    this.submitModel = Object.assign({}, this.model)
    this.submitModel["countryCode"]="+91";

    const loading = await this.loadingController.create({
      message: 'Checking...'
    });
    await loading.present();
    
    await this.api.checkUser(this.submitModel)
    .subscribe(res => {
      loading.dismiss();
        if(res['success']){
          this.sendOTP();
          this.router.navigate(['/otp', { mobileNumber: this.submitModel["mobileNumber"], pageFrom:"LoginWithOTP"} ]);
        }else {    
          this.router.navigate([ '/registration', { mobileNumber: this.submitModel["mobileNumber"]} ]);
        }
      }, (err) => {
        console.log(err);
        loading.dismiss();
      });
  }

  async sendOTP(){
  
    await this.api.sendOtp(this.submitModel)
    .subscribe(res => {
        if(res['success']){
          console.log("Sent");
        }
        else {    
          this.presentAlertConfirm('Unable to send OTP !!');
        }
      }, (err) => {
        console.log(err);
      });

  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  
  }

}
