import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from 'src/config/config';


const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

@Injectable({
    providedIn: 'root'
  })
  export class LoginServices {

    constructor(private http: HttpClient) { }
 
    checkUser (product): Observable<any> {
      const url = `${config.isMobileExist}`;
      return this.http.post<any>(url, product, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }

    sendOtp(jsonModel): Observable<any> {
      const url = `${config.sendOTP}`;
      return this.http.post<any>(url, jsonModel, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }   
  }