import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../app/profile/profile-api.service';
import { Data } from "../../app/_service/registration.service";
import { ProfileStorage } from "../../app/_service/profile.service";
import { LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit  {

  accountId:any  
  submitModel:any = {};
  model ={}

  constructor(route:ActivatedRoute,public loadingController: LoadingController, private data: Data, private profileData: ProfileStorage, public api: ProfileService,public router: Router) { }

  ngOnInit() { 

    //this.getProfile();
  }

  ionViewWillEnter() {
   // console.log('all done loading :)')
   this.getProfile();
  }

  async getProfile(){
    
    const loading = await this.loadingController.create({
      message: 'Getting profile...'
    });
    await loading.present(); 
    await this.data.getId().subscribe(res=>{
      this.api.getProfileById(res)
      .subscribe(res => {
        loading.dismiss();       
          if(res['success']){          
            this.model=res["data"];
          }               
        }, (err) => {
          console.log(err);        
          loading.dismiss();
        });
    });       
  }

  openEditProfile(){
    this.submitModel = Object.assign({}, this.model)
    this.profileData.setProfileData(this.submitModel);
    this.router.navigate(['/edit-profile']);
  }

  openChangeMobile(){
    this.router.navigate(['/change-mobile-number', { mobileNumber: this.model["accountId"]["mobileNumber"]}]);
  }
}
