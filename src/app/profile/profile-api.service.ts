import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

@Injectable({
    providedIn: 'root'
  })
  export class ProfileService {

    constructor(private http: HttpClient) { }
 
    getProfileById (id): Observable<any> {
      const url = `${config.getProfileByAccountId}${id}`;
      return this.http.get<any>(url, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }
  }