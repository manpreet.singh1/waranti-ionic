import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroscreenPage } from './introscreen.page';

describe('IntroscreenPage', () => {
  let component: IntroscreenPage;
  let fixture: ComponentFixture<IntroscreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroscreenPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroscreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
