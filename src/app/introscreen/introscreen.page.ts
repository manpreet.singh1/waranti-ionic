import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-introscreen',
  templateUrl: './introscreen.page.html',
  styleUrls: ['./introscreen.page.scss'],
})
export class IntroscreenPage implements OnInit {
  constructor(public menuCtrl: MenuController) { }
  lastSlide:boolean;
  slides:any;

  ngOnInit() {
    this.slides = document.querySelector('ion-slides');
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  slideOpts = {
    effect: 'flip',
    autoplay:true
  };
  goToNextSlide(){
    this.slides.slideNext(700);
  }  
  slideChanged(){
   this.slides.isEnd().then(res => this.lastSlide=res);
  }
}
