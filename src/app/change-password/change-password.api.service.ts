import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

@Injectable({
    providedIn: 'root'
  })
  export class ChangePasswordServices {

    constructor(private http: HttpClient) { }
 
    changePassword (id, product): Observable<any> {
      const url = `${config.changePassword}${id}`;
      return this.http.put<any>(url, product, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }
  }