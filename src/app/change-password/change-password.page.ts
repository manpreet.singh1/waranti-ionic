import { Component, OnInit } from '@angular/core';
import { ChangePasswordServices } from '../../app/change-password/change-password.api.service';
import { Data } from "../../app/_service/registration.service";
import { LoadingController, AlertController,NavController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  id:any;
  model:any = {};
  submitModel:any = {};

  constructor(public navCtrl: NavController, private data: Data,public api: ChangePasswordServices,public loadingController: LoadingController,public router: Router,public alertController: AlertController) { }

  ngOnInit() {
    this.data.getId().subscribe(res=>{
      this.id=res;
    });
  }

  async changePassword(){

    this.submitModel = Object.assign({}, this.model)

    delete this.submitModel["confirmPassword"];

    console.log(this.submitModel);

    const loading = await this.loadingController.create({
      message: 'Updating...'
    });
    await loading.present();
    
    await this.api.changePassword(this.id,this.submitModel)
    .subscribe(res => {
      loading.dismiss();
        if(res['success']){
          this.router.navigate(['/login']);
        }
      }, (err) => {        
        console.log(err);
        loading.dismiss();
        this.presentAlertConfirm('Unable to change password.');
      });
  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  
  }

}
