import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject} from 'rxjs';  

@Injectable({
    providedIn: 'root'
})
export class Data {

    private storage = new BehaviorSubject<any>({});
    private id = new BehaviorSubject<any>({});
    private profile = new BehaviorSubject<any>({});    

    public constructor() { }

    setRegData(behave: any) { 
        this.storage.next(behave); 
    } 
    getRegData(): Observable<any> { 
        return this.storage.asObservable(); 
    }

    setId(behave: string) { 
        this.id.next(behave); 
    } 
    getId(): Observable<string> { 
        return this.id.asObservable(); 
    }
}