import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject} from 'rxjs'; 

@Injectable({
    providedIn: 'root'
})
export class ProfileStorage {

    private storage = new BehaviorSubject<any>({});
    private userMobile = new BehaviorSubject<any>({});

    public constructor() { }

    setProfileData(behave: any) { 
        this.storage.next(behave); 
    } 
    getProfileData(): Observable<any> { 
        return this.storage.asObservable(); 
    }

    setUserMobile(behave: any) { 
        this.userMobile.next(behave); 
    } 
    getUserMobile(): Observable<any> { 
        return this.userMobile.asObservable(); 
    }
}