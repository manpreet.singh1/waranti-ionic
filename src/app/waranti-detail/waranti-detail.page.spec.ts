import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarantiDetailPage } from './waranti-detail.page';

describe('WarantiDetailPage', () => {
  let component: WarantiDetailPage;
  let fixture: ComponentFixture<WarantiDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarantiDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarantiDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
