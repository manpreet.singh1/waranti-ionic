import { Component } from '@angular/core';
import { MenuController, Platform } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(public menuCtrl: MenuController,  public platform: Platform) { }
  ngOnInit() {
    this.menuCtrl.enable(true);
  }
  slideOpts = {
    effect: 'flip',
    autoplay:true
  };
}
