import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfileStorage } from "../../app/_service/profile.service";
import { EditProfileServices } from '../../app/edit-profile/edit-profile.api.service';
import { Data } from "../../app/_service/registration.service";
import { LoadingController, AlertController,NavController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit,OnDestroy {

  id:any;
  model:any = {};
  submitModel:any = {};

  constructor(public navCtrl: NavController,private profileData: ProfileStorage, private data: Data,public api: EditProfileServices,public loadingController: LoadingController,public router: Router) { }

  ngOnInit() {
    this.profileData.getProfileData().subscribe(res=>{
      this.model=res;
    });

    this.data.getId().subscribe(res=>{
      this.id=res;
    });
  }
  ionViewDidLoad(){
    console.log("View edit");   
  }

  ngOnDestroy(){
  }

  async updateProfile(){

    this.submitModel = Object.assign({}, this.model)

    delete this.submitModel["_id"];
    delete this.submitModel["accountId"];
    delete this.submitModel["createdAt"];
    delete this.submitModel["updatedAt"];

    //console.log(this.submitModel);

    const loading = await this.loadingController.create({
      message: 'Updating...'
    });
    await loading.present();
    
    await this.api.updateProfile(this.id,this.submitModel)
    .subscribe(res => {
      loading.dismiss();
        if(res['success']){
         // this.profileData.setProfileData(res["data"]);
         //this.model=res["data"];
         this.router.navigate(['/profile']);
         
        }
      }, (err) => {
        console.log(err);
        loading.dismiss();
      });
  }
}
