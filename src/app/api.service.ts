import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Login } from './login';
import {environment} from '../environments/environment'


const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  @Injectable({
    providedIn: 'root'
  })
  export class ApiService {

    constructor(private http: HttpClient) { }
  
    private handleError<T> (operation = 'operation', result?: T) {
      return (error: any): Observable<T> => {
  
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead
  
        // Let the app keep running by returning an empty result.
        return of(result as T);
      };
    }
  
    // getProducts (): Observable<Login[]> {
    //   return this.http.get<Login[]>(apiUrl)
    //     .pipe(
    //       tap(heroes => console.log('fetched products')),
    //       catchError(this.handleError('getProducts', []))
    //     );
    // }
  
    // getProduct(id): Observable<Login> {
    //   const url = `${environment.apiUrl}/${id}`;
    //   return this.http.get<Login>(url).pipe(
    //     tap(_ => console.log(`fetched product id=${id}`)),
    //     catchError(this.handleError<Login>(`getProduct id=${id}`))
    //   );
    // }
  
    // addProduct (product): Observable<any> {
    //   const url = `${apiUrl}/login.php`;
    //   return this.http.post<any>(url, product, httpOptions).pipe(
    //     tap((product) => console.log(`added product w/ id=${product.id}`))
    //     );
    // }
  
    // updateProduct (id, product): Observable<any> {
    //   const url = `${apiUrl}/${id}`;
    //   return this.http.put(url, product, httpOptions).pipe(
    //     tap(_ => console.log(`updated product id=${id}`)),
    //     catchError(this.handleError<any>('updateProduct'))
    //   );
    // }
  
    // deleteProduct (id): Observable<Login> {
    //   const url = `${apiUrl}/${id}`;
  
    //   return this.http.delete<Login>(url, httpOptions).pipe(
    //     tap(_ => console.log(`deleted product id=${id}`)),
    //     catchError(this.handleError<Login>('deleteProduct'))
    //   );
    // }
  }