import { Component, OnInit } from '@angular/core';
import { ProfileStorage } from "../../app/_service/profile.service";
import { Data } from "../../app/_service/registration.service";
import { ChangeMobileServices } from '../../app/change-mobile-number/change-mobile-number.api.service';
import { LoadingController, AlertController,NavController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-change-mobile-number',
  templateUrl: './change-mobile-number.page.html',
  styleUrls: ['./change-mobile-number.page.scss'],
})
export class ChangeMobileNumberPage implements OnInit {
  
  model ={}  
  submitModel={}

  constructor(public navCtrl: NavController,public alertController: AlertController,private profileData: ProfileStorage, private data: Data,public api: ChangeMobileServices,public loadingController: LoadingController,public router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {    
    this.model["mobileNumber"]=this.activatedRoute.snapshot.paramMap.get('mobileNumber') || '';    
  }

  async verifyMobile(){   

    this.submitModel = Object.assign({}, this.model);
    this.submitModel["countryCode"]="+91";
    this.submitModel["mobileNumber"]=this.model["newMobileNumber"];
    delete this.submitModel["newMobileNumber"];
    console.log(this.submitModel);

    const loading = await this.loadingController.create({
      message: 'Updating...'
    });
    await loading.present();
    
    await this.api.checkUser(this.submitModel)
    .subscribe(res => {
      loading.dismiss();
        if(!res['success']){  
          this.sendOTP();
          this.router.navigate(['/otp', { mobileNumber: this.submitModel["mobileNumber"], pageFrom:"ChangeNumber"} ]);       
         }
      }, (err) => {
        this.presentAlertConfirm("Mobile number already exists !!");
        loading.dismiss();
      });
  }

  async sendOTP(){
  
    await this.api.sendOtp(this.submitModel)
    .subscribe(res => {
        if(res['success']){
          console.log("Sent");
        }
        else {    
          this.presentAlertConfirm('Unable to send OTP !!');
        }
      }, (err) => {
        console.log(err);
      });

  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  
  }

}
