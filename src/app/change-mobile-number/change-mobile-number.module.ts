import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChangeMobileNumberPage } from './change-mobile-number.page';

const routes: Routes = [
  {
    path: '',
    component: ChangeMobileNumberPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChangeMobileNumberPage]
})
export class ChangeMobileNumberPageModule {}
