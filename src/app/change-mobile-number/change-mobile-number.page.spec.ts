import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMobileNumberPage } from './change-mobile-number.page';

describe('ChangeMobileNumberPage', () => {
  let component: ChangeMobileNumberPage;
  let fixture: ComponentFixture<ChangeMobileNumberPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeMobileNumberPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeMobileNumberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
