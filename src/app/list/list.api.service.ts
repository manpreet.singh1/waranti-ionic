import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

@Injectable({
    providedIn: 'root'
  })
  export class WarrantiesList {

    constructor(private http: HttpClient) { }
 
    getWarrantyList (accountId): Observable<any> {
      const url = `${config.getWarranties}${accountId}`;
      return this.http.get<any>(url, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }

    getWarranty (id): Observable<any> {
        const url = `${config.getWarranty}${id}`;
        return this.http.get<any>(url, httpOptions).pipe(
          tap((product) => console.log(`${product}`))
          );
      }
  }