import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfileStorage } from "../../app/_service/profile.service";
import { WarrantiesList } from '../../app/list/list.api.service';
import { Data } from "../../app/_service/registration.service";
import { LoadingController, AlertController,NavController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  private selectedItem: any;
  id:any;
  openSearch:boolean =false;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  warranties=[];
  public items: Array<{ title: string; note: string; icon: string }> = [];
  constructor(public navCtrl: NavController,private profileData: ProfileStorage, private data: Data,public api: WarrantiesList,public loadingController: LoadingController,public router: Router) {
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  ngOnInit() {  
    
  }

  ionViewWillEnter(){
    this.data.getId().subscribe(res=>{
      this.id=res;
      this.getWarranties();
    });
  }

  toggleSearch(){
    this.openSearch = !this.openSearch;
  }

  async getWarranties(){

    const loading = await this.loadingController.create({
      message: 'Getting waranties...'
    });
    await loading.present();
    
    await this.api.getWarrantyList(this.id)
    .subscribe(res => {
      loading.dismiss();
        if(res['success']){
        this.warranties=res["data"];
        }
      }, (err) => {
        console.log(err);
        loading.dismiss();
      });

  }
}
