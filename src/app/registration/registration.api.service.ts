import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from '../../environments/environment'
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  @Injectable({
    providedIn: 'root'
  })
  export class RegistrationService {

    constructor(private http: HttpClient) { }
   
    sendOtp(jsonModel): Observable<any> {
      const url = `${config.sendOTP}`;
      return this.http.post<any>(url, jsonModel, httpOptions).pipe(
        tap((product) => console.log(`${product.success}`))
        );
    }    
  }