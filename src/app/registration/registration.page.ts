import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';
import { RegistrationService } from '../../app/registration/registration.api.service';
import { ActivatedRoute,Router } from '@angular/router';
import { Data } from "../../app/_service/registration.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  model= {}
  submitModel={};
  redirectModel={};
  mobileNumber = null;

  constructor(public alertController: AlertController, private data: Data,private activatedRoute: ActivatedRoute,public menuCtrl: MenuController,public api: RegistrationService,public loadingController: LoadingController,  public router: Router) { }

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.mobileNumber = this.activatedRoute.snapshot.paramMap.get('mobileNumber');
    this.model["mobileNumber"]=this.mobileNumber;
  }

 async registerUser() {
    this.submitModel = Object.assign({}, this.model)
    delete this.submitModel['confirmPassword'];
    delete this.submitModel['name'];
    delete this.submitModel['password'];
    this.submitModel["countryCode"]="+91";

    //console.log(this.submitModel);
    //console.log(this.model);

    const loading = await this.loadingController.create({
      message: 'Registering...'
    });
   await loading.present();
    
    await this.api.sendOtp(this.submitModel)
    .subscribe(res => {
        loading.dismiss();
        if(res['success']){
          this.redirectModel = Object.assign({}, this.model)
          this.redirectModel["countryCode"]=this.submitModel["countryCode"];
         // console.log(this.redirectModel);
          this.data.setRegData(Object.assign({}, this.redirectModel));
          //this.data.storage=Object.assign({}, this.redirectModel);
          this.router.navigate([ '/otp',{pageFrom:"Registration"}]);
        }else {    
          this.presentAlertConfirm('The provided mobile number is not valid');
        }
      }, (err) => {
        console.log(err);
        loading.dismiss();
      });
  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  }

}
