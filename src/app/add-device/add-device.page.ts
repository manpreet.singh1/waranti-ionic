import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonApiService } from "../../app/_common-api-service/common.api.service";
import { AddDeviceService } from '../../app/add-device/add-device.api.service';
import { Data } from "../../app/_service/registration.service";
import { LoadingController, AlertController,NavController} from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileStorage } from "../../app/_service/profile.service";


@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.page.html',
  styleUrls: ['./add-device.page.scss'],
})
export class AddDevicePage implements OnInit {

  productModel={}
  id:any;
  userMobileNumber:any
  submitModel={}
  categories=[];
  dropCategory="Serial No."

  constructor(private profileData: ProfileStorage,public alertController: AlertController,private data: Data,public api: AddDeviceService,public commonApi: CommonApiService,public loadingController: LoadingController,public router: Router) { }

  ngOnInit() {
    this.profileData.getUserMobile().subscribe(res=>{
     this.userMobileNumber=res;
    });
    this.data.getId().subscribe(res=>{
      this.id=res;
    });
    this.getCategories();
  }

  async getCategories(){
    const loading = await this.loadingController.create({
      message: 'Getting categories...'
    });
    await loading.present();
    
    await this.commonApi.getCategories()
    .subscribe(res => {
      loading.dismiss();
        if(res['success']){    
         this.categories = res["data"];
        }
      }, (err) => {
        console.log(err);
        loading.dismiss();
      });
  }

  async addDevice(){

//this.submitModel["mobileNumber"]=this.userMobileNumber;
    this.submitModel["accountId"]=this.id;
    this.submitModel["product"]=this.productModel;

    const loading = await this.loadingController.create({
      message: 'Adding device...'
    });
    await loading.present();
    
    await this.api.addDevice(this.submitModel)
    .subscribe(res => {
        loading.dismiss();
        if(res['success']){    
          console.log("Added"); 
          this.router.navigate(['/list']);       
        }
      }, (err) => {
        this.presentAlertConfirm("Device already exists !!")
        loading.dismiss();
      });
  }
  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  
  }

  onCategorySelected(){
    if( this.categories[this.categories.findIndex(x => x._id==this.productModel['category'])]['name']=='Smartphone'){
      this.dropCategory='IMEI No.';
    }else{
      this.dropCategory='Serial No.';
    }
  }
}
