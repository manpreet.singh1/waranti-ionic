import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

@Injectable({
    providedIn: 'root'
  })
  export class AddDeviceService {

    constructor(private http: HttpClient) { }
 
    addDevice (product): Observable<any> {
      const url = `${config.addDevice}`;
      return this.http.post<any>(url, product, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }
  }