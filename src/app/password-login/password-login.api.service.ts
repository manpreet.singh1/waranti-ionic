import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from '../../environments/environment'
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

@Injectable({
    providedIn: 'root'
  })
  export class PasswordLoginServices {

    constructor(private http: HttpClient) { }
 
  
    // getProducts (): Observable<Login[]> {
    //   return this.http.get<Login[]>(apiUrl)
    //     .pipe(
    //       tap(heroes => console.log('fetched products')),
    //       catchError(this.handleError('getProducts', []))
    //     );
    // }
  
    // getProduct(id): Observable<Login> {
    //   const url = `${environment.apiUrl}/${id}`;
    //   return this.http.get<Login>(url).pipe(
    //     tap(_ => console.log(`fetched product id=${id}`)),
    //     catchError(this.handleError<Login>(`getProduct id=${id}`))
    //   );
    // }
  
    loginUser (product): Observable<any> {
      const url = `${config.loginWithPassword}`;
      return this.http.post<any>(url, product, httpOptions).pipe(
        tap((product) => console.log(`${product.success}`))
        );
    }

    getProfileById (id): Observable<any> {
      const url = `${config.getProfileByAccountId}${id}`;
      return this.http.get<any>(url, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }
  
    // updateProduct (id, product): Observable<any> {
    //   const url = `${apiUrl}/${id}`;
    //   return this.http.put(url, product, httpOptions).pipe(
    //     tap(_ => console.log(`updated product id=${id}`)),
    //     catchError(this.handleError<any>('updateProduct'))
    //   );
    // }
  
    // deleteProduct (id): Observable<Login> {
    //   const url = `${apiUrl}/${id}`;
  
    //   return this.http.delete<Login>(url, httpOptions).pipe(
    //     tap(_ => console.log(`deleted product id=${id}`)),
    //     catchError(this.handleError<Login>('deleteProduct'))
    //   );
    // }
  }