import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordLoginPage } from './password-login.page';

describe('PasswordLoginPage', () => {
  let component: PasswordLoginPage;
  let fixture: ComponentFixture<PasswordLoginPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordLoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordLoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
