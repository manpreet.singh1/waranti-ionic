import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { ActivatedRoute,Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Data } from "../../app/_service/registration.service";
import { ProfileStorage } from "../../app/_service/profile.service";
import { PasswordLoginServices } from '../../app/password-login/password-login.api.service';

@Component({
  selector: 'app-password-login',
  templateUrl: './password-login.page.html',
  styleUrls: ['./password-login.page.scss'],
})
export class PasswordLoginPage implements OnInit {
  model= {}
  constructor(private profileData: ProfileStorage,public alertController: AlertController, private data: Data,public menuCtrl: MenuController, public router: Router,public api: PasswordLoginServices,public loadingController: LoadingController) { }

  ngOnInit() {
    this.menuCtrl.enable(false);
  }

 async loginUser() {
    this.model["countryCode"]="+91";
    console.log(this.model);
    const loading = await this.loadingController.create({
      message: 'Loading...'
    });
    await loading.present();    
    await this.api.loginUser(this.model)
    .subscribe(res => {
      loading.dismiss();       
      if(res['success']){
        this.data.setId(res['data']['_id']);
        this.profileData.setUserMobile(this.model["mobileNumber"]);
        this.router.navigate(['/home']);  
        this.getProfile();
      }               
      }, (err) => {
        console.log(err);
        this.presentAlertConfirm('Invalid credentials !');
        loading.dismiss();
      });
  }

  async getProfile(){
    
    await this.data.getId().subscribe(res=>{
      this.api.getProfileById(res)
      .subscribe(res => {
          if(res['success']){          
            this.profileData.setProfileData(res["data"]);
            // this.model=res["data"];
          }               
        }, (err) => {
          console.log(err);        
        });
    });       
  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });

    await alert.present();
  }

}
