import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from '../../environments/environment'
import { config } from 'src/config/config';

const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  @Injectable({
    providedIn: 'root'
  })
  
  export class VerifyOTPServices {

    constructor(private http: HttpClient) { 

    }

    loginWithOtp (product): Observable<any> {
        const url = `${config.loginWithOTP}`;
        return this.http.post<any>(url, product, httpOptions).pipe(
          tap((product) => console.log(`${product}`))
          );
      }

      verifyOtp (product): Observable<any> {
        const url = `${config.verifyOTP}`;
        return this.http.post<any>(url, product, httpOptions).pipe(
          tap((product) => console.log(`${product}`))
          );
      }

    registerUser (product): Observable<any> {
      const url = `${config.registerUser}`;
      return this.http.post<any>(url, product, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }

    getProfileById (id): Observable<any> {
      const url = `${config.getProfileByAccountId}${id}`;
      return this.http.get<any>(url, httpOptions).pipe(
        tap((product) => console.log(`${product}`))
        );
    }

  }