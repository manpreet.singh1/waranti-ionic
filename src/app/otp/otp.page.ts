import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { LoadingController, AlertController } from '@ionic/angular';
import { ActivatedRoute,Router } from '@angular/router';
import { VerifyOTPServices } from '../../app/otp/otp.api.service';
import { Data } from "../../app/_service/registration.service";
import { ProfileStorage } from "../../app/_service/profile.service";
import { ToastController } from '@ionic/angular';

  @Component({
    selector: 'app-otp',
    templateUrl: './otp.page.html',
    styleUrls: ['./otp.page.scss'],
  })
  export class OtpPage implements OnInit {

    model:any = {};
    pageFrom:any;
   
  constructor(public toastController: ToastController,private profileData: ProfileStorage,private data: Data,public alertController: AlertController,public api: VerifyOTPServices,public loadingController: LoadingController,public menuCtrl: MenuController,  public router: Router, private activatedRoute: ActivatedRoute) { 
  }

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.pageFrom = this.activatedRoute.snapshot.paramMap.get('pageFrom');
    this.model["mobileNumber"] = this.activatedRoute.snapshot.paramMap.get('mobileNumber') || '';
    this.model["countryCode"] = "+91";
    if(this.pageFrom =="Registration"){
      this.data.getRegData().subscribe(res=>{
        this.model=res;
      });
    }
  } 

  async verifyOTP() {
    const loading = await this.loadingController.create({
      message: 'Verifying...'
    });
    await loading.present();
    if(this.pageFrom=="LoginWithOTP"){

      await this.api.loginWithOtp(this.model)
      .subscribe(res => {      
          if(res['success']){
           loading.dismiss();
           this.data.setId(res['data']['_id']);
           this.profileData.setUserMobile(this.model["mobileNumber"]);
           this.router.navigate(['/home']);  
           this.presentToast("Logged in successfully.");     
           this.getProfile();   
          }
        }, (err) => {
          this.presentAlertConfirm('Invalid OTP.');
          console.log(err);
          loading.dismiss();
        });
    }else if(this.pageFrom =="ChangeNumber"){
      await this.api.verifyOtp(this.model)
      .subscribe(res => {      
          if(res['success']){
           loading.dismiss();
           //this.data.setId(res['data']['_id']);
           this.presentLoginDialog("Mobile number changed successfully. Please login to continue.");           
          }
        }, (err) => {
          console.log(err);
          this.presentAlertConfirm('Invalid OTP.');
          loading.dismiss();
        });
    }else{
      delete this.model['confirmPassword'];
      await this.api.registerUser(this.model)
      .subscribe(res => {      
          if(res['success']){
           loading.dismiss();
           this.data.setId(res['data']['_id']);
           this.profileData.setUserMobile(this.model["mobileNumber"]);
           this.router.navigate(['/home']); 
           this.presentToast("Password successfully changed.");  
           this.getProfile();      
          }
        }, (err) => {
          this.presentAlertConfirm('Invalid OTP.');
          console.log(err);
          loading.dismiss();
        });
    }   
  }

  async getProfile(){
    
    await this.data.getId().subscribe(res=>{
      this.api.getProfileById(res)
      .subscribe(res => {
          if(res['success']){          
            this.profileData.setProfileData(res["data"]);
            // this.model=res["data"];
          }               
        }, (err) => {
          console.log(err);        
        });
    });       
  }

  async presentAlertConfirm(msg: string) {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            alert.dismiss();
          }
        }
      ]
    });
    await alert.present();
  
  }

  async presentLoginDialog(msg: string) {
    const alert = await this.alertController.create({
      header: 'Success!',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
           // alert.dismiss();
            this.router.navigate([ '/login']);  
          }
        }
      ]
    });
    await alert.present();
  
  }

  async presentToast(msg:string) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
