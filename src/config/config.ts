import {environment} from '../environments/environment'

export const config = {
    apiPath:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}`,
    isMobileExist:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/isAccountExistsWithMobileNumber`,
    sendOTP:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/send_otp`,
    verifyOTP:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/verify_otp`,
    loginWithOTP:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/login_with_otp`,
    registerUser:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/create`,
    loginWithPassword:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/login`,
    getProfileByAccountId:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/getPersonalInfoByAccountId/`,
    updateProfile:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/editPersonalInfoByAccountId/`,
    changePassword:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}account/changePasswordByAccountId/`,
    getCategories:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}master/getCategories`,
    addDevice:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}waranti/addDeviceByUser`,
    getWarranties:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}waranti/getWarantiesByAccountId/`,
    getWarranty:`${environment.apiBase}${environment.apiPath}${environment.apiVersion}waranti/getWarantiById/`
  };